-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 02:11 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coronaryintervention_faculty`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(50) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `city` varchar(200) NOT NULL,
  `hospital` varchar(150) NOT NULL,
  `User Action` varchar(200) NOT NULL,
  `eventname` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `FullName`, `email`, `city`, `hospital`, `User Action`, `eventname`) VALUES
(1, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', '13-03-2021 19:45', '13-03-2021 21:59'),
(2, 'Test', 'udit.mathur144@jtb-india.com', 'Gurgaon', 'Test', '13-03-2021 19:37', '13-03-2021 20:05'),
(3, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', '13-03-2021 19:50', '13-03-2021 19:52'),
(4, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', '13-03-2021 19:33', '13-03-2021 21:59'),
(5, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', '13-03-2021 19:51', '13-03-2021 19:52'),
(6, 'dr rajeshwari', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', '13-03-2021 19:37', '13-03-2021 19:41'),
(7, 'dr rajeshwari', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', '13-03-2021 19:40', '13-03-2021 19:40'),
(8, 'dr rajeshwari', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', '13-03-2021 19:41', '13-03-2021 19:43'),
(9, 'dr rajeshwari', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', '13-03-2021 19:44', '13-03-2021 21:59'),
(10, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', '13-03-2021 19:52', '13-03-2021 19:55'),
(11, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', '13-03-2021 19:55', '13-03-2021 21:59'),
(12, 'ilayaraja', 'druiraja@yahoo.com', 'chennai', 'billroth', '13-03-2021 19:57', '13-03-2021 21:59'),
(13, 'ASHA MAHILMARAN', 'drashamahil@gmail.com', 'CHENNAI', 'APOLLO HOSPITALS', '13-03-2021 20:00', '13-03-2021 21:59'),
(14, 'manokar panchanatham', 'pmanokar@hotmail.com', 'chennai', 'srmc', '13-03-2021 20:07', '13-03-2021 21:59'),
(15, 'drsundar', 'lathasundar07@gmail.com', 'Chennai', 'hariharan hospitals', '13-03-2021 20:08', '13-03-2021 21:59');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 17:13:38', '2021-03-12 17:13:38', '2021-03-12 17:14:08', 1, 'Abbott'),
(2, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 18:26:03', '2021-03-12 18:26:03', '2021-03-12 18:26:33', 1, 'Abbott'),
(3, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 18:26:03', '2021-03-12 18:26:03', '2021-03-12 18:26:33', 1, 'Abbott'),
(4, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021-03-13 13:56:57', '2021-03-13 13:56:57', '2021-03-13 13:57:27', 1, 'Abbott'),
(5, 'Test ', 'udit.mathur144@jtb-india.com', 'Gurgaon', 'Test', NULL, NULL, '2021-03-13 14:00:57', '2021-03-13 14:00:57', '2021-03-13 14:01:27', 1, 'Abbott'),
(6, 'ilayaraja', 'druiraja@yahoo.com', 'chennai', 'billroth', NULL, NULL, '2021-03-13 14:07:30', '2021-03-13 14:07:30', '2021-03-13 14:08:00', 1, 'Abbott'),
(7, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', NULL, NULL, '2021-03-13 15:36:11', '2021-03-13 15:36:11', '2021-03-13 15:36:41', 1, 'Abbott'),
(8, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-03-13 15:37:05', '2021-03-13 15:37:05', '2021-03-13 15:37:35', 1, 'Abbott'),
(9, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-03-13 16:09:05', '2021-03-13 16:09:05', '2021-03-13 16:09:35', 1, 'Abbott'),
(10, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', NULL, NULL, '2021-03-13 16:09:06', '2021-03-13 16:09:06', '2021-03-13 16:09:36', 1, 'Abbott'),
(11, 'Rithin', 'rithinraja.rajendran@abbott.com', 'Chennai', 'AV', NULL, NULL, '2021-03-13 19:28:12', '2021-03-13 19:28:12', '2021-03-13 19:28:42', 1, 'Abbott'),
(12, 'Test', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-03-13 19:30:36', '2021-03-13 19:30:36', '2021-03-13 19:31:06', 1, 'Abbott'),
(13, 'dr rajeshwari ', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', NULL, NULL, '2021-03-13 19:36:49', '2021-03-13 19:36:49', '2021-03-13 19:37:19', 1, 'Abbott'),
(14, 'Test', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-03-13 19:36:51', '2021-03-13 19:36:51', '2021-03-13 19:37:21', 1, 'Abbott'),
(15, 'dr rajeshwari ', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', NULL, NULL, '2021-03-13 19:39:46', '2021-03-13 19:39:46', '2021-03-13 19:40:16', 1, 'Abbott'),
(16, 'dr rajeshwari ', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', NULL, NULL, '2021-03-13 19:40:39', '2021-03-13 19:40:39', '2021-03-13 19:41:09', 1, 'Abbott'),
(17, 'dr rajeshwari ', 'drrajeshwari2006@yahoo.com', 'chennai', 'apollo', NULL, NULL, '2021-03-13 19:44:06', '2021-03-13 19:44:06', '2021-03-13 19:44:36', 1, 'Abbott'),
(18, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021-03-13 19:44:28', '2021-03-13 19:44:28', '2021-03-13 19:44:58', 1, 'Abbott'),
(19, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', NULL, NULL, '2021-03-13 19:49:48', '2021-03-13 19:49:48', '2021-03-13 19:50:18', 1, 'Abbott'),
(20, 'DR.K.P.SURESH KUMAR', 'kpscardio@yahoo.co.in', 'CHENNAI', 'KAUVERY HOSPITAL, CHENNAI', NULL, NULL, '2021-03-13 19:55:31', '2021-03-13 19:55:31', '2021-03-13 19:56:01', 1, 'Abbott'),
(21, 'ilayaraja', 'druiraja@yahoo.com', 'chennai', 'billroth', NULL, NULL, '2021-03-13 19:56:57', '2021-03-13 19:56:57', '2021-03-13 19:57:27', 1, 'Abbott'),
(22, 'ASHA MAHILMARAN', 'drashamahil@gmail.com', 'CHENNAI', 'APOLLO HOSPITALS', NULL, NULL, '2021-03-13 19:59:57', '2021-03-13 19:59:57', '2021-03-13 20:00:27', 1, 'Abbott'),
(23, 'manokar panchanatham', 'pmanokar@hotmail.com', 'chennai', 'srmc', NULL, NULL, '2021-03-13 20:06:37', '2021-03-13 20:06:37', '2021-03-13 20:07:07', 1, 'Abbott'),
(24, 'drsundar', 'lathasundar07@gmail.com', 'Chennai', 'hariharan hospitals', NULL, NULL, '2021-03-13 20:08:02', '2021-03-13 20:08:02', '2021-03-13 20:08:32', 1, 'Abbott');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
