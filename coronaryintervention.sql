-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 19, 2022 at 02:11 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coronaryintervention`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_questions`
--

INSERT INTO `tbl_questions` (`id`, `user_name`, `user_email`, `user_question`, `asked_at`, `eventname`, `speaker`, `answered`) VALUES
(1, 'Nishanth', 'nishanth@coact.co.in', 'Hello\r\n', '2021-03-12 14:33:53', 'Abbott', 0, 1),
(2, 'Reynold', 'reynold@coact.co.in', 'testing\r\n', '2021-03-12 17:05:21', 'Abbott', 0, 1),
(3, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'hi', '2021-03-12 17:06:46', 'Abbott', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-03-12 14:31:12', '2021-03-12 14:31:12', '2021-03-12 14:32:43', 0, 'Abbott', 'd89a6b62e902216937d10a4f1bd6010e'),
(2, 'akshat', 'neeraj@coact.co.in', 'nellore', 'bng', NULL, NULL, '2021-03-12 14:31:29', '2021-03-12 14:31:29', '2021-03-12 14:38:30', 0, 'Abbott', '415144e11a5e92dc92e1e9d2b5599659'),
(3, 'Nishanth', 'nishanth@coact.co.in', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-12 14:33:46', '2021-03-12 14:33:46', '2021-03-12 14:37:47', 0, 'Abbott', 'ba70d72f5a621bdc2b201d7164c0cd3a'),
(4, 'akshat', 'neeraj@coact.co.in', 'nellore', '9700467764', NULL, NULL, '2021-03-12 17:05:08', '2021-03-12 17:05:08', '2021-03-12 17:06:59', 0, 'Abbott', '39c441374da3eafeb9e1b6e0ead5088a'),
(5, 'V', 'viraj@coact.co.in', 'Pune', 'NH', NULL, NULL, '2021-03-12 17:05:11', '2021-03-12 17:05:11', '2021-03-12 17:06:02', 0, 'Abbott', '1b841bd1e6294f3153da23e7c5f0f4ab'),
(6, 'Akshat Jharia', 'akshatjharia@gmail.com', 'Bangalore', 'COACT', NULL, NULL, '2021-03-12 17:05:12', '2021-03-12 17:05:12', '2021-03-12 17:16:37', 0, 'Abbott', 'c0fa20c5f68ab6f2900f28033aee3220'),
(7, 'Reynold', 'reynold@coact.co.in', 'coact', 'coact', NULL, NULL, '2021-03-12 17:05:12', '2021-03-12 17:05:12', '2021-03-12 17:06:13', 0, 'Abbott', 'e5babe26133ca94e74a65e726c25c556'),
(8, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'Bangalore', 'Mg', NULL, NULL, '2021-03-12 17:05:14', '2021-03-12 17:05:14', '2021-03-12 17:05:54', 0, 'Abbott', '143bf31c0428638fd7e72e41d1400a8e'),
(9, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 17:05:15', '2021-03-12 17:05:15', '2021-03-12 17:19:45', 0, 'Abbott', 'c142dd9a8dfa4cea37e03dd7c33146f8'),
(10, 'PAWAN SHILWANT', 'pawan@coact.co.in', 'Bangalore', 'IT', NULL, NULL, '2021-03-12 17:05:30', '2021-03-12 17:05:30', '2021-03-12 17:06:41', 0, 'Abbott', '9e7b3a187bbf928c25ba5f1e7ef659bb'),
(11, 'janardhan towers', 'pawan@coact.co.in', 'Bangalore', 'IT', NULL, NULL, '2021-03-12 17:06:20', '2021-03-12 17:06:20', '2021-03-12 18:29:31', 0, 'Abbott', 'e4e431acabf9c75d7f44616fd9087450'),
(12, 'Reynold', 'reynold@coact.co.in', 'coact', 'coact', NULL, NULL, '2021-03-12 17:34:05', '2021-03-12 17:34:05', '2021-03-12 17:36:41', 0, 'Abbott', '5836242a52e27c44434d4ac83fe58778'),
(13, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 18:03:47', '2021-03-12 18:03:47', '2021-03-12 18:26:07', 0, 'Abbott', 'd61bda8850eed110bcb717e98cf82a09'),
(14, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-12 18:25:44', '2021-03-12 18:25:44', '2021-03-12 18:26:17', 0, 'Abbott', 'a9dccc72537ea1918742389ecec06ed4'),
(15, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bangalore', NULL, NULL, '2021-03-12 22:03:02', '2021-03-12 22:03:02', '2021-03-12 23:33:02', 0, 'Abbott', '61aade42fc0a0831215d84a83dec6490'),
(16, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bangalore', NULL, NULL, '2021-03-12 22:28:47', '2021-03-12 22:28:47', '2021-03-12 23:58:47', 0, 'Abbott', '7940d88a41f4508574e1a31fa582a635'),
(17, 'Test', 'Udit.mathur144@gmail.com', 'Test', 'Tes', NULL, NULL, '2021-03-13 19:26:02', '2021-03-13 19:26:02', '2021-03-13 19:55:47', 0, 'Abbott', '0518c201d817807e90c31184a67a83d3'),
(18, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'Bangalore', NULL, NULL, '2021-03-13 19:30:46', '2021-03-13 19:30:46', '2021-03-13 19:32:07', 0, 'Abbott', '48c7d230d26186a377ca8a35f40e9139'),
(19, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'coact', NULL, NULL, '2021-03-13 19:32:36', '2021-03-13 19:32:36', '2021-03-13 19:36:38', 0, 'Abbott', '1728e4be3e92becb5536552d3b33c623'),
(20, 'ilayaraja', 'druiraja@yahoo.com', 'chennai', 'billroth', NULL, NULL, '2021-03-13 19:53:38', '2021-03-13 19:53:38', '2021-03-13 19:55:38', 0, 'Abbott', '1682bf938b9a602378f3163b46427425'),
(21, 'Test', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-03-13 19:56:09', '2021-03-13 19:56:09', '2021-03-13 20:28:37', 0, 'Abbott', '7eac05cbb3dfefd482725902adb263c2'),
(22, 'Prasanna Murugesan', 'drprasanna98@yahoo.com', 'Hosur', 'Kauvery hospital', NULL, NULL, '2021-03-13 20:04:12', '2021-03-13 20:04:12', '2021-03-13 22:00:01', 0, 'Abbott', '62d7eedf1552ca92e38192a7ddc450f6'),
(23, 'manokar panchanatham', 'pmanokar@hotmail.com', 'chennai', 'srmc', NULL, NULL, '2021-03-13 20:05:30', '2021-03-13 20:05:30', '2021-03-13 21:35:30', 0, 'Abbott', '003c599d0cf909bfd9d124683622de6c'),
(24, 'manokar panchanatham', 'pmanokar@hotmail.com', 'chennai', 'srmc', NULL, NULL, '2021-03-13 20:05:31', '2021-03-13 20:05:31', '2021-03-13 20:06:53', 0, 'Abbott', '49556a4fc34b53cd38f9d2b45f3b50a5'),
(25, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-03-13 20:07:34', '2021-03-13 20:07:34', '2021-03-13 20:12:05', 0, 'Abbott', '3ce829556820a78c2aaeac8ece5dfe17'),
(26, 'Govind', 'govindaraj.kamaraj@abbott.com', 'Chennai ', 'Abbott Vascular ', NULL, NULL, '2021-03-13 20:09:29', '2021-03-13 20:09:29', '2021-03-13 22:11:07', 0, 'Abbott', '651a950aacb32a5f2bb57bc76548e99b'),
(27, 'Kasi', 'Kasirajaboopathy@gmail.com', 'Pondicherry ', 'Apollo', NULL, NULL, '2021-03-13 20:11:35', '2021-03-13 20:11:35', '2021-03-13 20:14:06', 0, 'Abbott', 'e7642882a5537ace2eca5f7c2a036f87'),
(28, 'Sriram', 'Srivats.vee@gmail.com', 'Chennai ', 'SRM', NULL, NULL, '2021-03-13 20:13:37', '2021-03-13 20:13:37', '2021-03-13 20:15:07', 0, 'Abbott', 'a6844ffdf29289bd48a305b2c3f90209'),
(29, 'Giridharan ', 'Girigirias@gmail.con', 'Pondicherry ', 'MGM', NULL, NULL, '2021-03-13 20:14:45', '2021-03-13 20:14:45', '2021-03-13 20:24:47', 0, 'Abbott', '712907d9f328656d7c96ede6358eee08'),
(30, 'Vinoth', 'vinothkumar123@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021-03-13 20:17:54', '2021-03-13 20:17:54', '2021-03-13 20:19:26', 0, 'Abbott', 'edff35cb433057a0a97daeabc167f13f'),
(31, 'Gowtham', 'rajigowthaman@gmail.com', 'Thiruvallur', 'MMM', NULL, NULL, '2021-03-13 20:19:03', '2021-03-13 20:19:03', '2021-03-13 21:20:56', 0, 'Abbott', 'bed610216c4f13892fe6e84988ba846c'),
(32, 'Britto David', 'brittodavis@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021-03-13 20:19:27', '2021-03-13 20:19:27', '2021-03-13 21:25:54', 0, 'Abbott', '7e7e9ad9f62ed3f90253bfd6598864dc'),
(33, 'Vishesh. S.P', 'vigneshsp@gmail.com', 'Chennai', 'Saveetha', NULL, NULL, '2021-03-13 20:20:24', '2021-03-13 20:20:24', '2021-03-13 21:30:46', 0, 'Abbott', '389920dc05b342381330e606268658a1'),
(34, 'Ganesh', 'ganesh078@gmail.com', 'Chennai', 'Billroth', NULL, NULL, '2021-03-13 20:21:16', '2021-03-13 20:21:16', '2021-03-13 21:30:59', 0, 'Abbott', '256933d766fc6510186ccfa69d8acc4f'),
(35, 'Karthik', 'karthikjacky@gmail.com', 'Chennai', 'Kauvery', NULL, NULL, '2021-03-13 20:22:33', '2021-03-13 20:22:33', '2021-03-13 21:15:37', 0, 'Abbott', '3bf2b7d436405a43ef620942d7e17a94'),
(36, 'Saren kumar', 'saren1993@gmail.com', 'Salem', '. ', NULL, NULL, '2021-03-13 20:23:09', '2021-03-13 20:23:09', '2021-03-13 21:20:47', 0, 'Abbott', '1d92d2fdcb96c6f3d036a848fd203315'),
(37, 'Vinodh', 'Vinodhkumar80@gmail.com', 'Chennai', 'Apollo', NULL, NULL, '2021-03-13 20:24:26', '2021-03-13 20:24:26', '2021-03-13 21:25:27', 0, 'Abbott', '05fc7c6b361e426560a7ee79621675d0'),
(38, 'Bala', 'balaraji@gmail.com', 'Coimbatore', '. ', NULL, NULL, '2021-03-13 20:24:42', '2021-03-13 20:24:42', '2021-03-13 21:26:03', 0, 'Abbott', '9718935bbee9f75d3edc978bea988127'),
(39, 'Vimal Kumar', 'Vimalrajjp@yahoo.co.in', 'Chennai ', 'PHC', NULL, NULL, '2021-03-13 20:24:57', '2021-03-13 20:24:57', '2021-03-13 20:30:48', 0, 'Abbott', '5e8e0e76aa87611252d0c36e56fa495d'),
(40, 'Preetam ', 'preetam.krish@gmail.com', 'Chennai ', 'SRMC ', NULL, NULL, '2021-03-13 20:25:16', '2021-03-13 20:25:16', '2021-03-13 21:15:16', 0, 'Abbott', '058560f421c15be21b1564fe9a0a0be1'),
(41, 'Prathap singh', 'prathapsingh@gmail.co', 'Madurai', '. ', NULL, NULL, '2021-03-13 20:25:39', '2021-03-13 20:25:39', '2021-03-13 21:40:33', 0, 'Abbott', 'd0ae28f36a1f93d131d6fc5b2c3cb3f8'),
(42, 'Srikumar ', 'ssrk96@yahoo.co.in', 'Chennai ', 'MIOT', NULL, NULL, '2021-03-13 20:25:54', '2021-03-13 20:25:54', '2021-03-13 21:26:46', 0, 'Abbott', '0ab99430d31ff36fc9bec46a47f67da7'),
(43, 'Arunesh', 'arunudaykumar123@gmail.com', 'Chennai', 'Miot', NULL, NULL, '2021-03-13 20:26:03', '2021-03-13 20:26:03', '2021-03-13 20:32:23', 0, 'Abbott', 'b266051efd699f5b0a975a9bf140b631'),
(44, 'Vikash ', 'vikash0089@gmail.com', 'Chennai ', 'SRMC ', NULL, NULL, '2021-03-13 20:26:18', '2021-03-13 20:26:18', '2021-03-13 21:27:15', 0, 'Abbott', '1a66c558114de7ce92fa9eefb0690eec'),
(45, 'Sivakumar ', 'shiva78.r@gmail.com', 'Chennai ', 'Billroth ', NULL, NULL, '2021-03-13 20:26:46', '2021-03-13 20:26:46', '2021-03-13 20:31:18', 0, 'Abbott', '29ed58dc096d117894cc64765179e3c9'),
(46, 'Test', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-03-13 20:29:56', '2021-03-13 20:29:56', '2021-03-13 21:13:17', 0, 'Abbott', '2bbdbc65c0b3cefaf4a206de3451deef'),
(47, 'Daniel k', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-03-13 20:29:59', '2021-03-13 20:29:59', '2021-03-13 20:30:51', 0, 'Abbott', '07ee37bb050f60556af73248f78a0daf'),
(48, 'Santhosh', 'santhoshkumar23@gmail.com', 'Chennai', 'Kauvery', NULL, NULL, '2021-03-13 20:30:28', '2021-03-13 20:30:28', '2021-03-13 21:15:21', 0, 'Abbott', '43c844f14e4f2ae93c5e9332ac45e55f'),
(49, 'Bino John ', 'drbinojohn@yahoo.co.in', 'Chennai ', 'Kamakshi Memorial ', NULL, NULL, '2021-03-13 20:30:49', '2021-03-13 20:30:49', '2021-03-13 21:32:45', 0, 'Abbott', 'c9fe65ce3f3098f69360d7811ef4d215'),
(50, 'Naseer', 'nadeer5627@gmail.com', 'Chennai', 'Promed', NULL, NULL, '2021-03-13 20:30:52', '2021-03-13 20:30:52', '2021-03-13 21:33:31', 0, 'Abbott', 'd3c20de79ea354eb4a5241c33ee691a1'),
(51, 'Umapathy ', 'vumapathy1@yahoo.com', 'Chennai ', 'Vijaya Hospital ', NULL, NULL, '2021-03-13 20:31:16', '2021-03-13 20:31:16', '2021-03-13 21:40:18', 0, 'Abbott', '152aeccb287bc7108e2abf8d9f697566'),
(52, 'Sanjay ', 'sanjaiinorion@gmail.com', 'Chennai ', 'SIMS ', NULL, NULL, '2021-03-13 20:31:49', '2021-03-13 20:31:49', '2021-03-13 21:30:00', 0, 'Abbott', 'b49e6f4f3275296f0d738e739aa334ad'),
(53, 'Rajkumar', 'rajkumar23@gmail.con', 'Coimbatore', 'SRH', NULL, NULL, '2021-03-13 20:32:01', '2021-03-13 20:32:01', '2021-03-13 21:20:53', 0, 'Abbott', 'cf77c5155ee39ff9ff25b17ec862d87c'),
(54, 'Basker', 'baskerraj6754@gmail.com', 'Chennai', 'Promed', NULL, NULL, '2021-03-13 20:32:23', '2021-03-13 20:32:23', '2021-03-13 21:40:24', 0, 'Abbott', '95b56b39f7623030eebe4da7d03f451b'),
(55, 'Viginesh ', 'vigineshthanikgaivasan@gmail.com', 'Chennai ', 'MIOT', NULL, NULL, '2021-03-13 20:32:35', '2021-03-13 20:32:35', '2021-03-13 21:33:40', 0, 'Abbott', '26a54aa2e85a8fe5e6d235eb0614f63b'),
(56, 'Danny', 'dannyroy45@gmail.com', 'Trichy', 'Apollo', NULL, NULL, '2021-03-13 20:32:57', '2021-03-13 20:32:57', '2021-03-13 21:40:53', 0, 'Abbott', '9887de88895b987f5b1ed81e80d790cb'),
(57, 'Krishnamurthy ', 'drakrishnamurthy@gmail.com', 'Chennai ', 'EVANS ', NULL, NULL, '2021-03-13 20:33:14', '2021-03-13 20:33:14', '2021-03-13 21:36:36', 0, 'Abbott', 'b41121b1347f9215feea3cacde5f6cb9'),
(58, 'Krishna', 'krishnamurthi@gmail.com', 'Coimbatore', 'PGS', NULL, NULL, '2021-03-13 20:33:23', '2021-03-13 20:33:23', '2021-03-13 21:34:34', 0, 'Abbott', 'dbd569a545bd0bb9fb48348f81b16242'),
(59, 'Shiyam ', 'Shiyam.sundar@abbott.com', 'Chennai ', 'AV', NULL, NULL, '2021-03-13 20:33:43', '2021-03-13 20:33:43', '2021-03-13 21:59:57', 0, 'Abbott', 'e6708960c77816e33fb278c9812fb653'),
(60, 'David', 'davidchristoper55@gmail.com', 'Chennai', 'MGM', NULL, NULL, '2021-03-13 20:34:08', '2021-03-13 20:34:08', '2021-03-13 21:25:14', 0, 'Abbott', 'a43b3de1ebe35d0d04337dcb88f28659'),
(61, 'Roy', 'roy12765@gmail.com', 'Chennai', 'Aakash', NULL, NULL, '2021-03-13 20:34:45', '2021-03-13 20:34:45', '2021-03-13 21:25:22', 0, 'Abbott', '63eee935205c07eca6af78d252201e59'),
(62, 'Senthil', 'senthilnathantth@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-03-13 20:39:49', '2021-03-13 20:39:49', '2021-03-13 20:45:19', 0, 'Abbott', '6938bb06251494fce42d3ef0fd0d6661'),
(63, 'Egambaram', 'ergambaram@gmail.com', 'Coimbatore', '. ', NULL, NULL, '2021-03-13 20:43:17', '2021-03-13 20:43:17', '2021-03-13 21:35:48', 0, 'Abbott', '5e6d1be39f6e77d8e62d7a18e7559fed'),
(64, 'Moorthy', 'moorthy55@gmail.com', 'Chennai', '. ', NULL, NULL, '2021-03-13 20:44:19', '2021-03-13 20:44:19', '2021-03-13 20:50:19', 0, 'Abbott', 'b28b470e8c9d600d8ef7d9dfb765156c'),
(65, 'Avinash', 'Avinbox@yahoo.com', 'Chennai ', 'Kims', NULL, NULL, '2021-03-13 20:44:52', '2021-03-13 20:44:52', '2021-03-13 21:47:07', 0, 'Abbott', 'c3b0262b1d83b09110e9e9a346916a7c'),
(66, 'KC Rathan ', 'Kamanatchanolian.rathan@abbott.com', 'Kamanatchanolian.rathan@abbott.com ', 'Av', NULL, NULL, '2021-03-13 20:55:46', '2021-03-13 20:55:46', '2021-03-13 21:03:08', 0, 'Abbott', '57e10261acc71c2308cc8a54a63db225'),
(67, 'Tamizha', 'Tamizha123@gmail.com', 'Salem', '. ', NULL, NULL, '2021-03-13 20:56:43', '2021-03-13 20:56:43', '2021-03-13 21:30:44', 0, 'Abbott', '007c7d292c4fe302947fcd172252d9b0'),
(68, 'Vikey. U', 'vickeevj@gmail.com', 'Chennai', '. ', NULL, NULL, '2021-03-13 20:57:20', '2021-03-13 20:57:20', '2021-03-13 20:59:41', 0, 'Abbott', 'a8a604449ce692009084597abd51b1a7'),
(69, 'Saikumar', 'saikumar78@gmail.com', 'Madurai', '. ', NULL, NULL, '2021-03-13 20:59:11', '2021-03-13 20:59:11', '2021-03-13 21:50:24', 0, 'Abbott', '123b368a2e82c339035b4d200d1a6c43'),
(70, 'Gopal ', 'gopalck@gmail.com', 'Salem', '. ', NULL, NULL, '2021-03-13 20:59:58', '2021-03-13 20:59:58', '2021-03-13 21:30:14', 0, 'Abbott', '6950596866128a4c9bd119ce75ba4e04'),
(71, 'Parthasarathy', 'partha45@gmail.com', 'Chennai', '. ', NULL, NULL, '2021-03-13 21:00:47', '2021-03-13 21:00:47', '2021-03-13 21:45:52', 0, 'Abbott', '36ad090c45afb81811c1e6cf5db94631'),
(72, 'Rohit A', 'a.rohit@abbott.com', 'Coimbatore ', 'AV', NULL, NULL, '2021-03-13 21:01:06', '2021-03-13 21:01:06', '2021-03-13 21:03:57', 0, 'Abbott', '5b980847764b25518c38caadd1ba9090'),
(73, 'Praveen kuamr', 'praveenkuamar@gmail.com', 'Madurai', '. ', NULL, NULL, '2021-03-13 21:02:25', '2021-03-13 21:02:25', '2021-03-13 21:38:08', 0, 'Abbott', 'fecb997439fe5367f141c421ffc28eb0'),
(74, 'David ', 'david56@gmail.com', 'Selam', '. ', NULL, NULL, '2021-03-13 21:05:40', '2021-03-13 21:05:40', '2021-03-13 21:09:01', 0, 'Abbott', 'f7bcea83be5b710c4d5bfa3b74c0d318'),
(75, 'Chandrasekar', 'chandru78@gmail.com', 'Chennai', '. ', NULL, NULL, '2021-03-13 21:08:35', '2021-03-13 21:08:35', '2021-03-13 21:42:41', 0, 'Abbott', '3394bbda1a8d2bc437306d59320af847'),
(76, 'Manoj', 'manoj45@gmail.com', 'Madurai', '. ', NULL, NULL, '2021-03-13 21:09:15', '2021-03-13 21:09:15', '2021-03-13 21:35:07', 0, 'Abbott', 'ca2dcc61e5ec9f34c336793ad38f9fed'),
(77, 'John', 'john65@gmail.com', 'Coimbatore', '. ', NULL, NULL, '2021-03-13 21:11:45', '2021-03-13 21:11:45', '2021-03-13 21:40:07', 0, 'Abbott', 'cd452d90fd1cbffcf3272048eae33abe'),
(78, 'Vinoth. T', 'vinothkumar@gmail.com', 'Chennai', '. ', NULL, NULL, '2021-03-13 21:12:41', '2021-03-13 21:12:41', '2021-03-13 21:19:00', 0, 'Abbott', '4ddfcbe00db4828dbf064e09c33bb730'),
(79, 'Test', 'udit.mathur144@jtb-india.com', 'Test', 'Test', NULL, NULL, '2021-03-13 21:13:11', '2021-03-13 21:13:11', '2021-03-13 21:46:52', 0, 'Abbott', '6d1cb455ac614ae30291c8658136618e'),
(80, 'Daniel k', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-03-13 21:29:06', '2021-03-13 21:29:06', '2021-03-13 21:38:28', 0, 'Abbott', '25179f1bc364ac5efcb545c6986ac321'),
(81, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bangalore', NULL, NULL, '2021-03-13 21:33:20', '2021-03-13 21:33:20', '2021-03-13 21:34:31', 0, 'Abbott', '4d6aae43c92b1f3f2144b540b715104f'),
(82, 'Nishanth S', 'nishu_8989@yahoo.com', 'Bangalore', 'bzbsk', NULL, NULL, '2021-03-13 21:37:42', '2021-03-13 21:37:42', '2021-03-13 21:44:38', 0, 'Abbott', 'eb9119fc36150767544f349e8efa7a2f'),
(83, 'Nishanth', 'nishanth@coact.com', 'BAngalore', 'bangalore', NULL, NULL, '2021-03-16 11:57:32', '2021-03-16 11:57:32', '2021-03-16 12:00:53', 0, 'Abbott', '26737b110676460841fbc7dcf71c0135'),
(84, 'Naga neeraj', 'naganeerajreddy9989@gmail.com', 'Nellore', 'Bng', NULL, NULL, '2021-03-16 12:51:24', '2021-03-16 12:51:24', '2021-03-16 12:57:24', 0, 'Abbott', '2cf2ce9ccccfefa84990d9673f6d4050'),
(85, 'Viraj', 'viraj@coact.co.in', 'Pune', 'NH', NULL, NULL, '2021-07-02 13:41:33', '2021-07-02 13:41:33', '2021-07-02 15:11:33', 1, 'Abbott', '688d2fcaad89dedce65f80a6b2f7c3e0');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
